import { useQuery } from "react-query";
import axios from "axios";

const getAllRowsById = async (documentId) => {
  const { data } = await axios.get(
    `https://ads-compta.herokuapp.com/api/document/${documentId}/row`
  );
  return data;
};
export default function useGetAllRowsById(documentId) {
  return useQuery(["getRowById", getAllRowsById], () => getAllRowsById(documentId)); 
}
