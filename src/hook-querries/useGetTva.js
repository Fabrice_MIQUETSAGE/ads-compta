import { useQuery } from "react-query";
import axios from "axios";

const getTva = async () => {
  const { data } = await axios.get(
    "https://ads-compta.herokuapp.com/api/tva/"
  );
  return data;
};

export default function useGetTva() {
  return useQuery("getTva", getTva);
}
