import axios from 'axios'
import { useMutation, queryCache } from 'react-query'

export default function useDelDoc() {
  return useMutation(
    (docId) => axios.delete(`https://ads-compta.herokuapp.com/api/document/${docId}`).then((res) => res.data),
    {
      onSuccess: () => queryCache.refetchQueries('docs'),
    }
  )
}