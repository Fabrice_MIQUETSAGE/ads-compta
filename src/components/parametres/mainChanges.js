import { useState } from 'react'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'

export default function MainChanges() {

    const [site, setSite] = useState('ANNECY');
    const [exercice, setExercice] = useState('exe2021');
    const siteHandleclick = () => {
       // console.log('clic sur enregistrer site')
    }
    const exeHandleclick = () => {
       // console.log('clic sur enregistrer exercice')
    }

    const siteHandleChange = (event) => {
        setSite(event.target.value);
    };
    const exerciceHandleChange = (event) => {
        setExercice(event.target.value);
    };
    const sites = [
        {
            value: 'ANNECY',
            label: 'ANNECY',
        },
        {
            value: 'CLUSES',
            label: 'CLUSES',
        },
        {
            value: 'PARIS',
            label: 'PARIS',
        },

    ];
    const comptableExercice = [
        {
            value: '01/01/2019 - 31/12/2019',
            label: 'exe2019',
        },
        {
            value: '01/01/2020 - 31/12/2020',
            label: 'exe2020',
        },
        {
            value: '01/01/2021 - 31/12/2021',
            label: 'exe2021',
        },

    ];

    return (
        <Grid item container xs={10} direction="row"
        justify="flex-start">
            <Paper style={{ padding: "15px",marginRight:'20px',  backgroundColor: '#aecafc', width:'40%' }}>
                <h2>Changement de site</h2>
                <div style={{ textAlign: "center" }}>
                    <TextField
                        label="select-site"
                        id="select-site"
                        select
                        value={site}
                        onChange={siteHandleChange}
                        SelectProps={{
                            native: true,
                        }}
                        style={{ fontSize: "1.2em", fontWeight: "bolder", width: '100%' }}
                    >
                        {sites.map((option) => (
                            <option style={{ fontSize: "1.2em", fontWeight: "bolder" }} key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </TextField>
                </div>
                <Button onClick={siteHandleclick} variant="outlined" color="primary" size="small" style={{ marginTop: "20px" }}>enregistrer site</Button>
            </Paper >
            <Paper style={{ padding: "15px", backgroundColor: '#aecafc', width:'40%' }}>
                <h2 style={{ display: 'inline-block' }}>Changement d'exercice</h2>
                <div style={{ textAlign: "center" }}>
                    <TextField
                        label="select-exercice"
                        id="select-exercice"
                        select
                        value={exercice}
                        onChange={exerciceHandleChange}
                        SelectProps={{
                            native: true,
                        }}
                        style={{ width: '100%' }}
                    >
                        {comptableExercice.map((option) => (
                            <option style={{ fontSize: "1.2em", fontWeight: "bolder" }} key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </TextField>
                </div>
                <Button onClick={exeHandleclick} variant="outlined" color="primary" size="small" style={{ marginTop: "20px" }}>enregistrer exercice</Button>
            </Paper >


        </Grid>

    )
}