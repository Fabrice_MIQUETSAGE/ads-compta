import { render, screen } from '../../test-utils/test-utils'
import MainChanges from './mainChanges'

test('composant mainChange is ready', () => {
    render(<MainChanges />)

    //doit avoir un titre comprenant le mot site
    const mainChangesSiteTitle = screen.getByRole("heading", { name: /site$/i })
    expect(mainChangesSiteTitle).toBeInTheDocument();
    //doit avoir un input avec le nom select site
    const mainChangesSiteInput = screen.getByRole('combobox', { name: /site$/i })
    expect(mainChangesSiteInput).toBeInTheDocument();
    //doit avoir un bouton de validation
    const maincChangesSiteButton = screen.getByRole('button', {name: /site$/i})
    expect(maincChangesSiteButton).toBeInTheDocument();

    
    //doit avoir un titre comprenant le mot exercice
    const mainChangesExeTitle = screen.getByRole("heading", { name: /exercice$/i })
    expect(mainChangesExeTitle).toBeInTheDocument();
    //doit avoir un input avec le nom select exercice
    const mainChangesExeInput = screen.getByRole('combobox', { name: /site$/i })
    expect(mainChangesExeInput).toBeInTheDocument();
    //doit avoir un bouton de validation
    const maincChangesExeButton = screen.getByRole('button', {name: /exercice$/i})
    expect(maincChangesExeButton).toBeInTheDocument();
})