import { render, screen } from '../../test-utils/test-utils';
import Header from './header';

test('header has link', () => {
  render(<Header />);

  //doit avoir une image
  const headerImg = screen.getByRole('img', {name: 'logoApp'})
  expect(headerImg).toBeInTheDocument();
  
  //doit avoir une liste liens nomme ul link
  const headerLinkList = screen.getByRole('list', {name: 'ul_link'})
  expect(headerLinkList).toBeInTheDocument() 
  
  //doit avoir un liste nommé ul_infos
  const headerInfosList = screen.getByRole('list', {name: 'ul_infos'})
  expect(headerInfosList).toBeInTheDocument();
});
