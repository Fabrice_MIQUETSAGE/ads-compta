import React from 'react'
import { NavLink } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import logo from '../../logoapp2.png';
import "./header.scss"

export default function Header() {
    return (
        <div>
            <AppBar position="static">
                <Toolbar>
                    <img src={logo} style={{ width: "5%", paddingTop: '10px', paddingBottom: '10px' }} alt="logoApp" />
                    <ul className="header__ul" title="ul_link">
                        <NavLink activeStyle={{ color: "orange" }} to='/documents'>devis/facture</NavLink>
                        <NavLink activeStyle={{ color: "orange" }} to='/client'>Client</NavLink>
                        <NavLink activeStyle={{ color: "orange" }} to='/site'>Site</NavLink>
                        <NavLink activeStyle={{ color: "orange" }} to='/exercice'>Exercice</NavLink>
                        <NavLink activeStyle={{ color: "orange" }} to='/parametres'>Paramètres</NavLink>
                    </ul>
                    <ul className="header__ul2" title="ul_infos">
                        <li>Utilisateur : ADMIN</li>
                        <li>Site courant : ANNECY</li>
                        <li>Exercice Comptable : 01/01/2021-31/12/2021</li>
                    </ul>
                </Toolbar>
            </AppBar>

        </div>

    )
}