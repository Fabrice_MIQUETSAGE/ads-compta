import Grid from '@material-ui/core/Grid'


export default function footer (){
    return (
     <Grid 
     item 
     style={{height:'100px', backgroundColor: "#3F51B5", position:"absolute", width:'100%', bottom:'0', color:"white", textAlign:"center", paddingTop:"10px", fontSize:"0.8em"}}>
         tous droits réservés
     </Grid>   
    )   
}