
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'


export default function ExerciceList() {


    const handleclick = () => {
       // console.log('clic sur nouveau site')
    }
    const selectHandleclick = () => {
       // console.log('clic sur select exercice')
    }


    return (

        <Grid item xs={12} style={{ paddingTop: "50px", paddingBottom: '50px' }}>
            <Grid item align="left">
                <Button onClick={handleclick} href="/parametres" variant="contained" color="primary" style={{ marginBottom: "20px" }}>créer exercice</Button>
            </Grid>
            <TableContainer>
                <Table stickyHeader aria-label="table exercice">
                    <TableHead>
                        <TableRow>
                            <TableCell>EXERCICE COMPTABLE</TableCell>
                            <TableCell>DEBUT EXERCICE</TableCell>
                            <TableCell>FIN EXERCICE</TableCell>
                            <TableCell></TableCell>
                        </TableRow>

                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>EXE2021</TableCell>
                            <TableCell>01/01/2021</TableCell>
                            <TableCell>31/12/2021</TableCell>
                            <TableCell><Button onClick={selectHandleclick} variant="outlined" color="primary" size="small">sélectionner</Button>
                            </TableCell>
                        </TableRow> 
                        <TableRow>
                            <TableCell>EXE2020</TableCell>
                            <TableCell>01/01/2020</TableCell>
                            <TableCell>31/12/2020</TableCell>
                            <TableCell><Button onClick={selectHandleclick} variant="outlined" color="primary" size="small">sélectionner</Button>
                            </TableCell>
                        </TableRow>                 
                    </TableBody>
                </Table>
            </TableContainer>
            

        </Grid>
    )
}