import {render, screen} from '../../test-utils/test-utils'
import ClientList from './clientList'

test('ClientList is ready', ()=> {
    render(<ClientList/>)
    const ClientListTable = screen.getByRole("table", {name :"table client"})
    expect(ClientListTable).toBeInTheDocument();
});