
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'


export default function ClientDetailsHeader({details}) {
    return (
        <Paper style={{ padding: "20px", width: "100%", marginBottom: "50px" }}>
            <Grid
                item
                container
                justify="space-between"
                alignItems="flex-start"
                >
                <Grid item xs={4}>
                    <img src={`${details.imgLogo}`} alt="logo" style={{maxWidth:"80%"}} />
                </Grid>
                <Grid item xs={8}>
                    <h1>{details.company}</h1>
                    <p>CONTACT: {details.contactName}</p>
                    <p>EMAIL: <a href={`mailto: ${details.email}`}>{details.email}</a></p>
                    <p>TELEPHONE: <a href={`callto: ${details.phoneNumber}`}>{details.phoneNumber}</a></p>
                    <p>ADRESSE: {details.street}</p>
                    <p>CP: {details.zipCode}</p>
                    <p>VILLE: {details.city}</p>
                    <p>PAYS: FRANCE</p>
                </Grid>
            </Grid >
        </Paper>

    )
}
