import { useState } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import RevertIcon from "@material-ui/icons/NotInterestedOutlined";
import IconButton from "@material-ui/core/IconButton";
import TextField from '@material-ui/core/TextField'
import { useMutation, useQueryClient } from 'react-query'
import axios from 'axios'
import { NavLink } from 'react-router-dom'

import { postClient } from '../../querries/querries'



export default function ClientList({ data }) {
  const queryClient = useQueryClient()
  const [editMode, setEditMode] = useState(false)
  const [previous, setPrevious] = useState({});
  const [rows, setRows] = useState(data)
//   useEffect(()=>{
// setRows(data)
//   }, [data])

const mutatePostClient = useMutation(postClient, {
  onSuccess: () => {
      queryClient.invalidateQueries('clients')
  },
})
  //const postClients = useMutation(newClient => axios.post('https://ads-compta.herokuapp.com/api/client', newClient))
  const patchClient = useMutation(existingClient => axios.patch(`https://ads-compta.herokuapp.com/api/client/${existingClient.id}`, existingClient))
  const deleteClient = useMutation(existingClient => axios.delete(`https://ads-compta.herokuapp.com/api/client/${existingClient.id}`, existingClient))

  const addHandleclick = () => {
    const newRow =
    {
      company: '',
      contactName: '',
      email: '',
      phoneNumber: '',
      imgLogo: '',
      street: '',
      zipCode: '',
      city: '',
      country: '',
    }
    setRows(rows.concat(newRow))
    setEditMode(!editMode)
  }
  const deleteHandleclick = (rowToDelete) => {
    deleteClient.mutate(rowToDelete)
    const rowsAfterDelete = rows.filter(row => row !== rowToDelete)
    setRows(rowsAfterDelete)
  }

  const onToggleEditMode = id => {
   // console.log(id)
    setEditMode(!editMode)
  };

  const onRevert = id => {
    const newRows = rows.map(row => {
      if (row.id === id) {
        return previous[id] ? previous[id] : row;
      }
      return row;
    });
    setRows(newRows);
    setPrevious(state => {
      delete state[id];
      return state;
    });
    onToggleEditMode(id);
  };

  const handleChange = (event, row) => {
    if (!previous[row.id]) {
      setPrevious(state => ({ ...state, [row.id]: row }));
    }
    const value = event.target.value;
    const name = event.target.name;
    const { id } = row
    const newRows = rows.map(row => {
      if (row.id === id) {
        return { ...row, [name]: value };
      }
      return row;
    });
    //console.log(newRows);
    setRows(newRows);


  };

  const onValidation = (row) => {

    setEditMode(!editMode)
    row.id ? patchClient.mutate(row)
      :
      mutatePostClient.mutate(row)
   // console.log("do a creation", row)
  }

if (!data)return <p>loading</p>

 
  return (
    

    <Grid item xs={12} style={{ paddingTop: "50px" }}>
      <Grid item align="left">
        <Button onClick={addHandleclick} variant="contained" color="primary" style={{ marginBottom: "20px" }}>Nouveau client</Button>
      </Grid>
      <TableContainer>
        <Table stickyHeader aria-label="table client">
          <TableHead>
            <TableRow>
              
              <TableCell>SOCIETE</TableCell>
              <TableCell>CONTACT</TableCell>
              <TableCell>E-MAIL</TableCell>
              <TableCell>TELEPHONE</TableCell>
              <TableCell>LOGO</TableCell>
              <TableCell>ADRESSE</TableCell>
              <TableCell>CP</TableCell>
              <TableCell>VILLE</TableCell>
              <TableCell>PAYS</TableCell>
              <TableCell>EDIT</TableCell>
              <TableCell>VOIR</TableCell>
              <TableCell>DELETE</TableCell>
            </TableRow>

          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.id}>
                {!editMode ? (
                  <>
                    
                    <TableCell>{row.company}</TableCell>
                    <TableCell>{row.contactName}</TableCell>
                    <TableCell><a href={`mailto: ${row.email}`}>{row.email}</a></TableCell>
                    <TableCell><a href={`callto: ${row.phoneNumber}`}>{row.phoneNumber}</a></TableCell>
                    <TableCell><img src={`${row.imgLogo}`} alt="logo" style={{ width: "100px" }} /></TableCell>
                    <TableCell>{row.street}</TableCell>
                    <TableCell>{row.zipCode}</TableCell>
                    <TableCell>{row.city}</TableCell>
                    <TableCell>{row.country}</TableCell>
                    <TableCell>
                      <IconButton
                        color="primary"
                        aria-label="edit"
                        onClick={() => onToggleEditMode(row.id)}
                      >
                        <EditIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      <NavLink to={`/client/${row.id}`}>
                        <IconButton
                          color="primary"
                        >
                          <VisibilityIcon />
                        </IconButton>
                      </NavLink>



                    </TableCell>
                    <TableCell>
                      <IconButton
                        color="primary"
                        onClick={event => deleteHandleclick(row)}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </>
                ) : (
                  <>
                   
                    <TableCell><TextField name='company' value={row.company} onChange={event => handleChange(event, row)}>{row.company}</TextField></TableCell>
                    <TableCell><TextField name='contactName' value={row.contactName} onChange={event => handleChange(event, row)}>{row.contactName}</TextField></TableCell>
                    <TableCell><TextField name='email' value={row.email} onChange={event => handleChange(event, row)}>{row.email}</TextField></TableCell>
                    <TableCell><TextField name='phoneNumber' value={row.phoneNumber} onChange={event => handleChange(event, row)}>{row.phoneNumber}</TextField></TableCell>
                    <TableCell><TextField name='imgLogo' value={row.imgLogo} onChange={event => handleChange(event, row)}>{row.imgLogo}</TextField></TableCell>
                    <TableCell><TextField name='street' value={row.street} onChange={event => handleChange(event, row)}>{row.street}</TextField></TableCell>
                    <TableCell><TextField name='zipCode' value={row.zipCode} onChange={event => handleChange(event, row)}>{row.zipCode}</TextField></TableCell>
                    <TableCell><TextField name='city' value={row.city} onChange={event => handleChange(event, row)}>{row.city}</TextField></TableCell>
                    <TableCell><TextField name='country' value={row.country} onChange={event => handleChange(event, row)}>{row.country}</TextField></TableCell>
                    <TableCell>
                      <IconButton
                        color="primary"
                        aria-label="done"
                        onClick={() => onValidation(row)}
                      >
                        <DoneIcon />
                      </IconButton>
                      <IconButton
                        color="primary"
                        aria-label="revert"
                        onClick={() => onRevert(row.id)}
                      >
                        <RevertIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell><a href="/client/0001">
                      <IconButton
                        color="primary">
                        <VisibilityIcon />
                      </IconButton></a>
                    </TableCell>
                    <TableCell>
                      <IconButton
                        color="primary"
                        onClick={deleteHandleclick}>
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </>
                )}
              </TableRow>
            ))}

          </TableBody>
        </Table>
      </TableContainer>

    </Grid>

  )

}
