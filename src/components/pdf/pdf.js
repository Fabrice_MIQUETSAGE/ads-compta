import React from 'react'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import { useQuery } from 'react-query'
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import { useParams } from "react-router-dom"
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { fetchDocById } from '../../querries/querries'
import Pdf from "react-to-pdf";

export default function PdfPreview() {
    const { id } = useParams()
    let docToPrint = React.createRef()
   
    const { data: docData, status: docStatus } = useQuery(['docById', id], () => fetchDocById(id))
    const tvaArray = [
        {
            "id": 1,
            "taux": 20
        },
        {
            "id": 2,
            "taux": 10
        }
    ]
    const tauxTVA = (id) => {
        if (id) {
            return (tvaArray.filter(tva => tva.id === id)[0].taux)
        }
        else { return 0 }

    }



    if (docStatus === 'loading') return <p>loading data</p>
    if (docStatus === 'error') return <p>error fetching data</p>

    const clientDetails = docData.slice()[3][0]
    const rows = docData.slice()[2]
    const data = rows

    const subTotalHT_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.subTotalPrice
    }, 0)
    const subTotalHT_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.subTotalPrice
    }, 0)
    const totalTVA20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + (20 * val.totalPriceHT / 100)
    }, 0)
    const totalTVA10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + (10 * val.totalPriceHT / 100)
    }, 0)
    const totalDiscount10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + (val.discount * val.subTotalPrice / 100)
    }, 0)
    const totalDiscount20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + (val.discount * val.subTotalPrice / 100)
    }, 0)
    const totalHT_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.totalPriceHT
    }, 0)
    const totalHT_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.totalPriceHT
    }, 0)
    const totalTTC_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.totalPriceTTC
    }, 0)
    const totalTTC_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.totalPriceTTC
    }, 0)
    return (
        <>
        <Grid container justify="center" alignItems="center">
            <Grid item xs={2} style={{margin:'20px'}} >
                <Pdf targetRef={docToPrint} filename={docData[0].docType_id === 2 ? 'DEVIS-ANNECY-00' + docData[0].id : 'FACTURE-ANNECY-00' + docData[0].id}>
                    {({ toPdf }) => <Button style={{ marginRight: '10px' }} color="primary" variant="contained" onClick={toPdf}><PictureAsPdfIcon style={{paddingRight:'20px'}} />  enregistrer en pdf</Button>}
                </Pdf>
            </Grid>
            

            </Grid>
            <Grid
                item xs={5}
                container
                alignItems="center"
                justify="space-between"
                ref={docToPrint}
                style={{padding: '20px'}}>

                <Grid item xs={5} align="left" style={{ border: '1px solid grey', padding: '10px', borderRadius: '20px' }}>
                    <h2 style={{ fontSize: "1.1em", fontWeight: "bolder", textTransform: "uppercase" }}>ANNECY Industry</h2>
                    <div style={{ fontSize: "0.6em"}}>
                    <p>1 rue de la place</p>
                    <p>74000 ANNECY</p>
                    <p>France</p>
                    <p>SIRET: 00120125486</p> 
                    </div>
                    
                </Grid>

                <Grid item xs={5} style={{ border: '1px solid grey', padding: '10px', borderRadius: '20px' }}>

                    <h2 style={{ fontSize: "1.1em", fontWeight: "bolder", textTransform: "uppercase" }}>{clientDetails.company}</h2>
                    <div style={{ fontSize: "0.6em"}}>
                    <p>{clientDetails.street}</p>
                    <p>{clientDetails.zipCode}</p>
                    <p>{clientDetails.city}</p>
                    <p>{clientDetails.country}</p>
                    <p style={{ fontStyle: 'italic' }}>{clientDetails.email}</p>
                    </div>
                    
                </Grid>
                <h2>{docData[0].docType_id === 2 ? 'DEVIS-ANNECY-00' + docData[0].id : 'FACTURE-ANNECY-00' + docData[0].id}</h2>

                <TableContainer>
                    <Table size="small" stickyHeader aria-label="devfac table">
                        <TableHead>
                            <TableRow>
                                {/* <TableCell> </TableCell> */}
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>ID PRODUIT</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>PRODUIT</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>P.U €(HT)</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>QTE</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>S/TOTAL HT</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>REMISE %</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL €(HT)</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TAUX TVA</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL €(TTC)</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => (
                                <TableRow key={row.id}>
                                    <>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.apiProductId}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.apiProductName}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.apiProduct_unitPrice}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.quantity}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.subTotalPrice = row.apiProduct_unitPrice * row.quantity}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.discount}%</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.totalPriceHT = row.subTotalPrice - (row.subTotalPrice * row.discount / 100)}</TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{tauxTVA(row.tva_id)}%
                                        </TableCell>
                                        <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{row.totalPriceTTC = row.totalPriceHT + (row.totalPriceHT * tauxTVA(row.tva_id) / 100)}</TableCell>
                                    </>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TableContainer style={{ marginTop: "50px" }}>
                    <Table stickyHeader size="small" aria-label="devfac sum table">
                        <TableHead>
                            <TableRow>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TVA</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>S/TOTAL HT</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL REMISE</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL HT</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL TVA</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TOTAL TTC</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TVA à 20%</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{subTotalHT_20}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalDiscount20}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalHT_20}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalTVA20}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalTTC_20}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>TVA à 10%</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{subTotalHT_10}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalDiscount10}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalHT_10}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalTVA10}</TableCell>
                                <TableCell style={{fontSize:'0.7em', padding:'5px'}}>{totalTTC_10}</TableCell>
                            </TableRow>
                            <TableRow style={{ backgroundColor: "#b5c0ff" }} >
                                <TableCell style={{ fontWeight: "bolder", fontSize: "1em" }} align="right" colSpan={5} >TOTAL€ TTC</TableCell>
                                <TableCell style={{ fontWeight: "bolder", fontSize: "1em" }}>{totalTTC_10 + totalTTC_20}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>

                <footer style={{ textAlign: 'center', border: '1px solid grey', padding: '10px', borderRadius: '5px', marginTop: '50px', width: '100%', bottom: '0' }}>
                    <p style={{ fontSize: '0.6em', color: 'grey' }}>ANNECY INDUSTRY - 1 rue de la place - 74000 ANNECY - France - SIRET: 00120125486 - contact: contact@annecyIndustry.fr // tel: 0450505050</p>
                </footer>





                {/* </div> */}

            </Grid>


        </>


    )

}