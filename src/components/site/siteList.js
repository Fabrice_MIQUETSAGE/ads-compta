import { useState } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField';

export default function ClientList() {

    const [exercice, setExercice] = useState('exe2021');

    const handleChange = (event) => {
        setExercice(event.target.value);
    };
    const handleclick = () => {
       // console.log('clic sur nouveau client')
    }
    const selectHandleclick = () => {
       // console.log('clic sur select site')
    }

    const comptableExercice = [
        {
            value: '01/01/2019 - 31/12/2019',
            label: 'exe2019',
        },
        {
            value: '01/01/2020 - 31/12/2020',
            label: 'exe2020',
        },
        {
            value: '01/01/2021 - 31/12/2021',
            label: 'exe2021',
        },

    ];

    return (

        <Grid item xs={12} style={{ paddingTop: "50px", paddingBottom: '50px' }}>
            <Grid item align="left">
                <Button onClick={handleclick} href="/parametres" variant="contained" color="primary" style={{ marginBottom: "20px" }}>Nouveau site</Button>
            </Grid>
            <TableContainer>
                <Table stickyHeader aria-label="site liste">
                    <TableHead>
                        <TableRow>
                            <TableCell>CODE</TableCell>
                            <TableCell>SITE</TableCell>
                            <TableCell>LOGO</TableCell>
                            <TableCell>EXERCICE COMPTABLE</TableCell>
                            <TableCell></TableCell>
                        </TableRow>

                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>00001</TableCell>
                            <TableCell>ANNECY</TableCell>
                            <TableCell><img style={{maxWidth:"100px"}} src="https://zupimages.net/up/21/17/43bg.png" alt="logo" /></TableCell>
                            <TableCell>
                                <TextField
                                    id="select-exercice"
                                    select
                                    value={exercice}
                                    onChange={handleChange}
                                    SelectProps={{
                                        native: true,
                                    }}
                                >
                                    {comptableExercice.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </TableCell>
                            <TableCell><Button onClick={selectHandleclick} variant="outlined" color="primary" size="small">sélectionner</Button>
                            </TableCell>
                          

                        </TableRow>
                        <TableRow>
                            <TableCell>00002</TableCell>
                            <TableCell>CLUSES</TableCell>
                            <TableCell><img style={{maxWidth:"120px"}} src="https://zupimages.net/up/21/17/grmm.png" alt="logo" /></TableCell>
                            <TableCell>
                                <TextField
                                    id="select-exercice"
                                    select
                                    value={exercice}
                                    onChange={handleChange}
                                    SelectProps={{
                                        native: true,
                                    }}
                                >
                                    {comptableExercice.map((option) => (
                                        <option key={option.value} value={option.value}>
                                            {option.label}
                                        </option>
                                    ))}
                                </TextField>
                            </TableCell>
                            <TableCell><Button onClick={selectHandleclick} variant="outlined" color="primary" size="small">sélectionner</Button>
                            </TableCell>
                            <TableCell></TableCell>

                        </TableRow>

                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    )
}