import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
//import Select from "react-select";
import TextField from "@material-ui/core/TextField";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import { useHistory } from "react-router-dom";
import {apilogin} from './apilogin'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function LogForm(props) {
  const history = useHistory();
  let loginSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.number().required()
  });
  const [openSnack, setOpenSnack] = useState(false);

  const handleCloseSnack = (event, reason) => {
    if (reason === 'clickaway') {
      setOpenSnack(false)
    }
    setOpenSnack(false)
  }
  const {
    handleSubmit,
    control
  } = useForm({
    resolver: yupResolver(loginSchema)
  });

//test
    const onSubmit = async (data) => {
    try {
      const token = await apilogin(data.email, data.password)
      localStorage.setItem("jwt", token)
      
      history.push("/documents");
    } catch (error) {
      console.error(error);
      alert("Error logging in please try again");
    }
  };

  return (


    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={2} justify="center">
        <Grid item xs={12}>
          <Controller
            render={({ field, formState }) => (
              <TextField
                {...field}
                label="votre e-mail"
                error={!!formState.errors?.email}
                helperText={!!formState.errors?.email && "votre mail n'est pas valide"}
                variant="outlined"
              />
            )}
            name="email"
            control={control}
            defaultValue=""
          />
        </Grid>
        <Grid item xs={12}>
          <Controller
            render={({ field, formState }) => (
              <TextField
                {...field}
                label="votre mot de passe"
                error={!!formState.errors?.password}
                helperText={!!formState.errors?.password && "votre mot de passe n'est pas valide'"}
                variant="outlined"
              />
            )}
            name="password"
            control={control}
            defaultValue=""
          />
        </Grid>
        <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnack}>
          <MuiAlert onClose={handleCloseSnack} severity="error">
            email ou mot de passe erroné
        </MuiAlert>
        </Snackbar>

        {/* <Grid item xs={4}>
          <Controller
            name="site"
            control={control}
            render={({ field }) => <Select
              {...field}
              options={options}
              defaultValue={{
      id: "1",
      label: "ANNECY",
    }}
            />}
          />

        </Grid> */}

      </Grid>
      <Button variant="contained" color="primary" type="submit" style={{ marginTop: '20px' }}>Valider</Button>
      <p style={{color:'grey', fontSize:'1em', marginTop:'20px'}}>identifiants démonstration </p>
      <p style={{color:'grey', fontSize:'0.8em'}}>email : admin@admin.com</p>
      <p style={{color:'grey', fontSize:'0.8em'}}>mot de passe : 1234</p>

    </form>

  );
}

export default LogForm;