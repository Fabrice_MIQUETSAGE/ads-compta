import {render, screen} from '../../test-utils/test-utils'
import DevFac from './devFac'

test('document page is ready', ()=> {
    render(<DevFac/>)
    
    const devfacTable = screen.getByRole("table", {name: 'devfac table'})
    expect(devfacTable).toBeInTheDocument();

    const devfacSumTable = screen.getByRole("table", {name: 'devfac sum table'})
    expect(devfacSumTable).toBeInTheDocument();
})
