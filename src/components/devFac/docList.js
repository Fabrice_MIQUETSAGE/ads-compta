import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import VisibilityIcon from '@material-ui/icons/Visibility'
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from '@material-ui/core/Grid';
import { NavLink } from 'react-router-dom'
import IconButton from "@material-ui/core/IconButton";
import { useMutation, useQueryClient } from 'react-query'
import DayJS from 'react-dayjs';


//import querries 
import { deleteDoc } from '../../querries/querries'


export default function DocList({ documents }) {
console.log("doc form doclist",  documents)
    const queryClient = useQueryClient()
    const mutateDelete = useMutation(deleteDoc, {
        onSuccess: () => {
            queryClient.invalidateQueries('docs')

        },
    })

    const handleDelete = (doc) => {
        mutateDelete.mutate(doc.id)
    }

    // console.log(documents)
    if (!documents) { return (<p>loading...</p>) }
    else {
        return (
            <Grid item xs={12}>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>TYPE</TableCell>
                                <TableCell>NUMERO</TableCell>
                                <TableCell>SITE</TableCell>
                                <TableCell>CLIENT</TableCell>
                                <TableCell>DATE</TableCell>
                                <TableCell>STATUT</TableCell>
                                <TableCell>AUTEUR</TableCell>
                                <TableCell>TOTAL €(TTC)</TableCell>
                                <TableCell>VOIR</TableCell>
                                <TableCell>SUPPRESSION</TableCell>
                            </TableRow>

                        </TableHead>
                        <TableBody>
                            {documents.map(doc => (
                                <TableRow key={doc.id}>
                                    <TableCell style={{ textTransform: 'uppercase', fontWeight: 'bolder', color: '#303f9f' }}>{doc.document_type}</TableCell>
                                    <TableCell>{doc.document_type}-ANNECY-{doc.id}</TableCell>
                                    <TableCell>ANNECY</TableCell>
                                    <TableCell>{doc.client_company}</TableCell>
                                    <TableCell><DayJS format="DD-MM-YYYY">{doc.created_at}</DayJS></TableCell>
                                    <TableCell>-</TableCell>
                                    <TableCell>{doc.user_name}</TableCell>
                                    <TableCell>{doc.totalTTC.toFixed(2)}</TableCell>

                                    <TableCell>
                                        <NavLink to={`/document/${doc.id}`}>
                                            <IconButton color="primary"><VisibilityIcon /></IconButton>
                                        </NavLink>
                                    </TableCell>

                                    <TableCell>
                                        <IconButton color="primary" onClick={event => handleDelete(doc)}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        ) 
    }
}
