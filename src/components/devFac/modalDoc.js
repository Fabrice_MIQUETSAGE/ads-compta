
import { useState } from 'react'
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import { useHistory } from 'react-router-dom'
import { useQuery, useMutation, useQueryClient } from 'react-query'
import { fectchDocType, fetchClients } from '../../querries/querries'
import axios from 'axios'


export default function ModalDoc() {
    const queryClient = useQueryClient()
    let history = useHistory()
    const { data: docTypeData, status: docTypeStatus } = useQuery('doctypes', fectchDocType)
    const { data: clientData, status: clientStatus } = useQuery('clients', fetchClients)
    
    const postDoc = useMutation((newDoc => axios.post('https://ads-compta.herokuapp.com/api/document/', newDoc).then(response => (history.push(`/document/${response.data[0].id}`)))
    ), {
        onSuccess: () => { queryClient.invalidateQueries('docs') },
    }
    )
  
    const [clientModal, setClientModal] = useState()
    const [typeModal, setTypeModal] = useState()
    const [openSelect, setOpenSelect] = useState(false)
    const [openSelectType, setOpenSelectType] = useState(false)
    const [open, setOpen] = useState(false);
    const newDoc = {
        "docType_id": typeModal,
        "client_id": clientModal,
        "user_id": 1,
    }
    const handleCreateDoc = (event) => {
      //  console.log(typeModal, clientModal)
        postDoc.mutate(newDoc)
    }
    const handleChange = (event) => {
        setClientModal(event.target.value);
    };
    const handleChangeType = (event) => {
        setTypeModal(event.target.value);
    };
    const handleClose = () => {
        setOpenSelect(false);
    };
    const handleCloseType = () => {
        setOpenSelectType(false);
    };

    const handleOpen = () => {
        setOpenSelect(true);
    };
    const handleOpenType = () => {
        setOpenSelectType(true);
    };
    const handleclickDocument = () => {
        setOpen(true)
        //postDocDevis.mutate(newDocDevis)
    }
    const handleCloseModal = () => {
        setOpen(false)

    }
    if (docTypeStatus === 'error' || clientStatus === 'error') return <p>error fetching data</p>
    if (docTypeStatus === 'loading' || clientStatus === 'loading') return <p>loading data</p>

    return (
        <>
            <Button variant="contained" color="primary" onClick={handleclickDocument}>Nouveau document</Button>
            <Modal
                open={open}
                disableAutoFocus={true}
                onClose={handleCloseModal}
                style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', border: '0px' }}
            >
                <div style={{
                    outline: 'none',
                    width: '500px',
                    height: "300px",
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    backgroundColor: "#303f9f",
                    boxShadow: '0px 0px 40px black',
                    borderRadius: '20px',
                    padding: '20px'
                }}>
                <div style={{borderBottom: '1px solid #fff', paddingBottom:'10px'}}>
                   <h4 style={{ color: "#fff" }}>Vous êtes connectés en tant que :</h4>
                <p style={{ color: "#fff", textTransform:'uppercase', fontWeight:'bolder', textAlign:'center'}}>admin</p> 
                </div>
                <div style={{paddingBottom:'10px', display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',}}>              
                    <h4 style={{ color: "#fff", margin:'10px' }}>veuillez sélectionner un client :</h4>
                    <FormControl >
                        <Select
                            labelId="select-client"
                            id="select-client"
                            open={openSelect}
                            onClose={handleClose}
                            onOpen={handleOpen}
                            onChange={handleChange}
                            style={{ backgroundColor: '#fff', width: '150px' }}
                        >
                            {clientData.map(data => (
                                <MenuItem key={data.id} value={data.id}>{data.company}</MenuItem>
                            ))}
                        </Select>
                    </FormControl> 
                    </div>
                    <div style={{paddingBottom:'10px', display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'column',}}>  

                    <h4 style={{ color: "#fff", margin:'10px' }}>veuillez sélectionner un type de document :</h4>

                    <FormControl >
                        <Select
                            labelId="open-select-type"
                            id="select-type"
                            open={openSelectType}
                            onClose={handleCloseType}
                            onOpen={handleOpenType}
                            onChange={handleChangeType}
                            style={{ backgroundColor: '#fff', width: '150px' }}
                        >
                            {docTypeData.map(data => (
                                <MenuItem key={data.id} value={data.id}>{data.type}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                      </div>
                    <Button style={{marginTop:'10px'}} variant="contained" color="default" onClick={handleCreateDoc}>créer le document</Button>
                </div>
            </Modal>
        </>

    )
}