import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'

import { fetchDocById, postRow } from '../../querries/querries'
import { useParams } from "react-router-dom"
import { useHistory } from 'react-router-dom'
import { useQuery, useMutation, useQueryClient } from 'react-query'
import axios from 'axios'

export default function DevFacHeader() {
    const queryClient = useQueryClient()
    let history = useHistory()
    const { id } = useParams()
    
    const { data: docData, status: docStatus } = useQuery(['docById', id], () => fetchDocById(id))
    
    const postDoc = useMutation((newDoc => axios.post('https://ads-compta.herokuapp.com/api/document/', newDoc)
    ), {
        onSuccess: (result) => {
            const newID = result.data[0].id;
            mutatePostRow.mutate({ documentId: newID, changes: docData.slice()[2].map(data => {
                return (
                    {
                        "apiProductId": data.apiProductId,
                        "apiProductName": data.apiProductName,
                        "apiProduct_unitPrice": data.apiProduct_unitPrice,
                        "comments": data.comments,
                        "discount": data.discount,
                        "position": data.position,
                        "quantity": data.quantity,
                        "subTotalPrice": data.subTotalPrice,
                        "totalPriceHT": data.totalPriceHT,
                        "totalPriceTTC": data.totalPriceTTC,
                        "tva_id": data.tva_id,
                        "document_id": newID
                    }
                )
            }
            ) })
            history.push(`/document/${newID}`)
        },
    })
    const mutatePostRow = useMutation(postRow, {
        onSuccess: () => {

            queryClient.invalidateQueries('getRowById', 'docs')
        },
    })  

    if (docStatus === 'loading') return <p>loading data</p>
    if (docStatus === 'error') return <p>error fetching data</p>

    const clientDetails = docData.slice()[3][0]
    const docInfo = docData.slice()[0]
    const newDoc = {
        "client_id": docInfo.client_id,
        "docType_id": 1,
        "totalDiscount": docInfo.totalDiscount,
        "totalHT": docInfo.totalHT,
        "totalTTC": docInfo.totalTT,
        "totalTva": docInfo.totalTva,
        "user_id": docInfo.user_id,
    }
   
    const handleChangeDoc = () => {
        postDoc.mutate(newDoc)
    
    }


    return (

        <Grid
            item
            container
            justify="space-between"
            alignItems="flex-start" >
            <Grid item xs={3} align="center">
                <Paper style={{ padding: "20px" }}>
                    <h2>SITE: ANNECY</h2>
                    <img style={{maxWidth:"200px"}}src="https://zupimages.net/up/21/17/43bg.png" alt="logo site" />
                </Paper>

            </Grid>
            <Grid item xs={5}>
                <Paper style={{ padding: "20px" }}>
                    <h2>{docData[0].docType_id === 2 ? 'DEVIS-ANNECY-00' + docData[0].id : 'FACTURE-ANNECY-00' + docData[0].id}</h2>
                    <Grid item container>
                       <Grid item xs={6}>
                     <img src={clientDetails.imgLogo} style={{width:'200px'}} alt="logo site" />   
                    </Grid>
                    <Grid item xs={6}>
                    <p style={{ fontSize: "1.8em", fontWeight: "bolder", textTransform: "uppercase" }}>{clientDetails.company}</p>
                    <p>{clientDetails.street}</p>
                    <p>{clientDetails.zipCode}</p>
                    <p>{clientDetails.city}</p>
                    <p>{clientDetails.country}</p> 
                    <p style={{fontStyle:'italic'}}>{clientDetails.email}</p>

                    </Grid> 
                    </Grid>
                    
                    
                    
                </Paper>
                <Grid align="right">
                    {docData[0].docType_id === 2 && <Button variant="outlined" onClick={handleChangeDoc} color="primary" style={{marginTop:'20px'}}>passer en facture</Button>}
                </Grid>
                
            </Grid>
        </Grid>
    )


}