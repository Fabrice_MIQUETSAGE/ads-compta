
import { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

export default function ModalApi({addApiPN}) {

    
    const [open, setOpen] = useState(false);

    const handleclickModal = () => {
        setOpen(true)
        //postDocDevis.mutate(newDocDevis)
    }
    const handleCloseModal = () => {
        setOpen(false)
    }
    const [keyword, setKeyword] = useState('')
    const [results, setResults] = useState([]);

    useEffect(() => {
        const search = async () => {
            const url = `https://app-67fd1eb3-90a0-421e-a58b-b56a3ce596c1.cleverapps.io/api/parts/search?keyword=${keyword}`
            const token = 'abcdef'
            const res = await fetch(url, {
                headers: {
                    'Authorization': 'Bearer ' + token,
                },
            });
            const data = await res.json()
            setResults(data.items);
        }
        if (keyword) {
            search()
        }
    }, [keyword])

const handleAddRef = (result) => {
    addApiPN(
        {
            apiProductId: result.name,
            apiProductName: result.description,
            apiProduct_unitPrice: result.price,
            quantity: 0,
            subTotalPrice: 0,
            discount: 0,
            totalPriceHT: 0,
            tva_id: '',
            totalPriceTTC: 0,
        }
    )
    setOpen(false)
}

    const searchResultsMapped = results.map(result => {
        return (

            <TableRow key={result.id}>
                <TableCell>{result.name}</TableCell>
                <TableCell>{result.description}</TableCell>
                <TableCell>{result.price}</TableCell>
                <TableCell><Button variant="contained" color="primary" size="small" onClick={()=>{handleAddRef(result)}}
                >
                ajouter
                </Button>
                </TableCell>
            </TableRow>
        )
    })
   

    return (
        <>
            <Button onClick={handleclickModal} variant="contained" color="primary" style={{ marginTop: "20px", marginLeft:'20px' }}>Ajouter une référence</Button>
            <Modal
                open={open}
                disableAutoFocus={true}
                onClose={handleCloseModal}
                style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', border: '0px' }}
            >
                <div
                    style={{
                        outline: 'none',
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        backgroundColor: "#303f9f",
                        boxShadow: '0px 0px 40px black',
                        borderRadius: '20px',
                        padding: '20px'
                    }}>
                    <div style={{
                        paddingBottom: '10px', display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column',
                    }}>
                        <h4 style={{ color: "#fff", margin: '10px' }}>saisissez une référence :</h4>

                        <TextField
                            //variant="filled"
                            style={{ backgroundColor: '#fff', padding: '10px', margin: "10px" }}
                            type="text"
                            placeholder="référence"
                            value={keyword}
                            onChange={e => setKeyword(e.target.value)}

                        />
                        {searchResultsMapped.length !==0 ? 
                        <TableContainer component={Paper}>
                            <TableHead>

                            </TableHead>
                            <TableRow>
                                <TableCell>ID PRODUIT</TableCell>
                                <TableCell>PRODUIT</TableCell>
                                <TableCell>P.U €(HT)</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                            {searchResultsMapped}
                        </TableContainer>
                        :
                        <></>

                        }
                        
                    </div>
                </div>
            </Modal>
        </>

    )
}