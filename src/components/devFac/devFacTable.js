import { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import RevertIcon from "@material-ui/icons/NotInterestedOutlined";
import IconButton from "@material-ui/core/IconButton";
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { useMutation, useQueryClient } from 'react-query'
import { patchRow, postRow, deleteRow } from '../../querries/querries'
import { useParams } from "react-router-dom"
import ModalApi from './modalApi'
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

export default function DevFacTable({ data }) {
    const queryClient = useQueryClient()
    const [editMode, setEditMode] = useState(false)
    const [previous, setPrevious] = useState({});
    const [rows, setRows] = useState(data)
    const { id: documentId } = useParams()

    const mutatePatchRow = useMutation(patchRow, {
        onSuccess: () => {
            queryClient.invalidateQueries('getRowById')
        },
    })
    const mutatePostRow = useMutation(postRow, {
        onSuccess: () => {
            queryClient.invalidateQueries('getRowById', 'docs')
        },
    })
    const mutateDeleteRow = useMutation(deleteRow, {
        onSuccess: () => {
            queryClient.invalidateQueries('getRowById')
        },
    })

    useEffect(() => { setRows(data) }, [data])

    const tauxTVA = (id) => {
        if (id) {
            return (tvaArray.filter(tva => tva.id === id)[0].taux)
        }
        else { return 0 }

    }
    const tvaArray = [
        {
            "id": 1,
            "taux": 20
        },
        {
            "id": 2,
            "taux": 10
        }
    ]

    const addHandleclick = () => {
        const newRow =
        {
            apiProductId: '',
            apiProductName: '',
            apiProduct_unitPrice: 0,
            quantity: 0,
            subTotalPrice: 0,
            discount: 0,
            totalPriceHT: 0,
            tva_id: '',
            totalPriceTTC: 0,
        }
        setRows(rows.concat(newRow))
        setEditMode(!editMode)
    }
    const addApiPN = (choice) => {
        const newRow = choice
        setRows(rows.concat(newRow))
        setEditMode(!editMode)
    }


    const deleteHandleclick = (rowToDelete) => {
        const rowID = rowToDelete.id
        mutateDeleteRow.mutate({ id: rowID, documentId: documentId })
        const rowsAfterDelete = rows.filter(row => row !== rowToDelete)
        setRows(rowsAfterDelete)
        setEditMode(false)
    }
    const onToggleEditMode = id => {
        setEditMode(!editMode)
    };
    const onRevert = id => {
        const newRows = rows.map(row => {
            if (row.id === id) {
                return previous[id] ? previous[id] : row;
            }
            return row;
        });
        setRows(newRows);
        setPrevious(state => {
            delete state[id];
            return state;
        });
        onToggleEditMode(id);
    };
    const handleChange = (event, row) => {
        if (!previous[row.id]) {
            setPrevious(state => ({ ...state, [row.id]: row }));
        }
        const value = event.target.value;
        const name = event.target.name;
        const { id } = row
        const newRows = rows.map(row => {
            if (row.id === id) {
                return {
                    ...row, [name]: value
                };
            }
            return row;
        });

        setRows(newRows);
    };

    const [openSnack, setOpenSnack] = useState(false);

    const handleCloseSnack = (event, reason) => {
        if (reason === 'clickaway') {
            setOpenSnack(false)
        }
        setOpenSnack(false)
    }

    const onValidEditMode = row => {
        if (row.tva_id === '') {
            setOpenSnack(true)
        }
        else if (isNaN(row.apiProduct_unitPrice) || isNaN(row.quantity) || isNaN(row.subTotalPrice) || isNaN(row.discount)) {
            setOpenSnack(true)
        }

        else {
            setEditMode(!editMode)
            const rowID = row.id
            row.id ?
                mutatePatchRow.mutate({ id: rowID, documentId: documentId, changes: row }) :
                mutatePostRow.mutate({ documentId: documentId, changes: row })
        }
    }

    return (

        <Grid item xs={12} style={{ paddingTop: "50px" }}>
            <TableContainer>
                <Table stickyHeader aria-label="devfac table">
                    <TableHead>
                        <TableRow>
                            {/* <TableCell> </TableCell> */}
                            <TableCell>ID PRODUIT</TableCell>
                            <TableCell>PRODUIT</TableCell>
                            <TableCell>P.U €(HT)</TableCell>
                            <TableCell>QTE</TableCell>
                            <TableCell>S/TOTAL HT</TableCell>
                            <TableCell>REMISE %</TableCell>
                            <TableCell>TOTAL €(HT)</TableCell>
                            <TableCell>TAUX TVA</TableCell>
                            <TableCell>TOTAL €(TTC)</TableCell>
                            <TableCell>EDIT</TableCell>
                            <TableCell>SUPP</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.id}>
                                {!editMode ? (
                                    <>
                                        <TableCell>{row.apiProductId}</TableCell>
                                        <TableCell>{row.apiProductName}</TableCell>
                                        <TableCell>{row.apiProduct_unitPrice}</TableCell>
                                        <TableCell>{row.quantity}</TableCell>
                                        <TableCell>{row.subTotalPrice = (row.apiProduct_unitPrice * row.quantity)}</TableCell>
                                        <TableCell>{row.discount}%</TableCell>
                                        <TableCell>{row.totalPriceHT = (row.subTotalPrice - (row.subTotalPrice * row.discount / 100))}</TableCell>
                                        <TableCell>{tauxTVA(row.tva_id)}%
                                        </TableCell>
                                        <TableCell>{row.totalPriceTTC = row.totalPriceHT + (row.totalPriceHT * tauxTVA(row.tva_id) / 100)}</TableCell>
                                        <TableCell>
                                            <IconButton
                                                color="primary"
                                                aria-label="edit"
                                                onClick={() => onToggleEditMode(row.id)}
                                            >
                                                <EditIcon />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            <IconButton
                                                color="primary"
                                                onClick={event => deleteHandleclick(row)}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </>
                                ) : (
                                    <>
                                        <TableCell><TextField name='apiProductId' value={row.apiProductId} onChange={event => handleChange(event, row)}>{row.apiProductId}</TextField></TableCell>
                                        <TableCell><TextField name='apiProductName' value={row.apiProductName} onChange={event => handleChange(event, row)}>{row.apiProductName}</TextField></TableCell>
                                        <TableCell><TextField name='apiProduct_unitPrice' value={row.apiProduct_unitPrice} onChange={event => handleChange(event, row)}>{row.apiProduct_unitPrice}</TextField></TableCell>
                                        <TableCell><TextField name='quantity' value={row.quantity} onChange={event => handleChange(event, row)}>{row.quantity}</TextField></TableCell>
                                        <TableCell name='subTotalPrice' value={row.subTotalPrice = row.apiProduct_unitPrice * row.quantity} onChange={event => handleChange(event, row)}>{row.subTotalPrice}</TableCell>
                                        <TableCell><TextField name='discount' value={row.discount} onChange={event => handleChange(event, row)}>{row.adresse}</TextField></TableCell>
                                        <TableCell name='totalPriceHT' value={row.totalPriceHT = row.subTotalPrice - (row.subTotalPrice * row.discount / 100)} onChange={event => handleChange(event, row)}>{row.totalPriceHT}</TableCell>
                                        <TableCell><TextField select name='tva_id' value={row.tva_id} onChange={event => handleChange(event, row)}>
                                            {tvaArray.map((tva) => (
                                                <MenuItem key={tva.id} value={tva.id}>
                                                    {tva.taux}
                                                </MenuItem>
                                            ))}
                                        </TextField></TableCell>
                                        <TableCell name='totalPriceTTC' value={row.totalPriceTTC = row.totalPriceHT + (row.totalPriceHT * tauxTVA(row.tva_id) / 100)} onChange={event => handleChange(event, row)}>{row.totalPriceTTC}</TableCell>
                                        <TableCell>
                                            <IconButton
                                                color="primary"
                                                aria-label="done"
                                                onClick={() => onValidEditMode(row)}
                                            >
                                                <DoneIcon />
                                            </IconButton>
                                            <IconButton
                                                color="primary"
                                                aria-label="revert"
                                                onClick={() => onRevert(row.id)}
                                            >
                                                <RevertIcon />
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            <IconButton
                                                color="primary"
                                                onClick={deleteHandleclick}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </>
                                )}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Grid item align="right">
                <Button onClick={addHandleclick} variant="contained" color="primary" style={{ marginTop: "20px" }}>ajouter une ligne libre</Button>
                <ModalApi addApiPN={addApiPN} />
            </Grid>
            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnack}>
                <MuiAlert onClose={handleCloseSnack} severity="error">
                    votre ligne n'est pas correctement renseignée
        </MuiAlert>
            </Snackbar>

        </Grid>

    )
}

DevFacTable.propTypes = {
    data: PropTypes.array,
    setTotalTVA10: PropTypes.func,
    setTotalTVA20: PropTypes.func,
}
