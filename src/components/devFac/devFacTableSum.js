import {useEffect} from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
//import useGetTva from '../../hook-querries/useGetTva'





export default function DevFacTableSum({data, setNewDoc}) {
  
 // const { status, data: tvaArray } = useGetTva()
   
    
    const subTotalHT_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.subTotalPrice
    }, 0)
    const subTotalHT_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.subTotalPrice
    }, 0)
    const totalTVA20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + (20 * val.totalPriceHT / 100)
    }, 0)
    const totalTVA10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + (10 * val.totalPriceHT / 100)
    }, 0)
    const totalDiscount10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + (val.discount * val.subTotalPrice / 100)
    }, 0)
    const totalDiscount20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + (val.discount * val.subTotalPrice / 100)
    }, 0)
    const totalHT_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.totalPriceHT
    }, 0)
    const totalHT_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.totalPriceHT
    }, 0)
    const totalTTC_20 = data.filter(row => row.tva_id === 1).reduce(function (acc, val) {
        return acc + val.totalPriceTTC
    }, 0)
    const totalTTC_10 = data.filter(row => row.tva_id === 2).reduce(function (acc, val) {
        return acc + val.totalPriceTTC
    }, 0)



useEffect(() => {
    setNewDoc({
        "totalHT": totalHT_20 + totalHT_10,
        "totalTva": totalTVA10 + totalTVA20,
        "totalDiscount": totalDiscount20 + totalDiscount10,
        "totalTTC": (totalTTC_20 + totalTTC_10),
    })
}, [setNewDoc, totalDiscount10, totalDiscount20, totalHT_10, totalHT_20, totalTTC_10, totalTTC_20, totalTVA10, totalTVA20])
 
// if (status === 'error') return <p>error fetching data</p>
// if (status === 'loading') return <p>loading data</p>
    
return (
        <Grid item xs={10} style={{ paddingTop: "50px" }}>
            <TableContainer>
                <Table stickyHeader aria-label="devfac sum table">
                    <TableHead>
                        <TableRow>
                            <TableCell>TVA</TableCell>
                            <TableCell>S/TOTAL HT</TableCell>
                            <TableCell>TOTAL REMISE</TableCell>
                            <TableCell>TOTAL HT</TableCell>
                            <TableCell>TOTAL TVA</TableCell>
                            <TableCell>TOTAL TTC</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                        <TableCell>TVA à 20%</TableCell>
                            <TableCell>{subTotalHT_20.toFixed(2)}</TableCell>
                            <TableCell>{totalDiscount20.toFixed(2)}</TableCell>
                            <TableCell>{totalHT_20.toFixed(2)}</TableCell>
                            <TableCell>{totalTVA20.toFixed(2)}</TableCell>
                            <TableCell>{totalTTC_20.toFixed(2)}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TVA à 10%</TableCell>
                            <TableCell>{subTotalHT_10.toFixed(2)}</TableCell>
                            <TableCell>{totalDiscount10.toFixed(2)}</TableCell>
                            <TableCell>{totalHT_10.toFixed(2)}</TableCell>
                            <TableCell>{totalTVA10.toFixed(2)}</TableCell>
                            <TableCell>{totalTTC_10.toFixed(2)}</TableCell>
                        </TableRow>
                        <TableRow style={{backgroundColor:"#b5c0ff"}} >
                            <TableCell style={{fontWeight:"bolder", fontSize:"1.2em"}} align="right" colSpan={5} >TOTAL€ TTC</TableCell>
                            <TableCell style={{fontWeight:"bolder", fontSize:"1.2em"}}>{(totalTTC_10+totalTTC_20).toFixed(2)}</TableCell>
                        </TableRow>                      
                    </TableBody>        
                </Table>
            </TableContainer>
            
           
           
            
        </Grid>

    )
}
