import React, { useState } from 'react'
import DevFacHeader from './devFacHeader'
import DevFacTable from './devFacTable'
import DevFacTableSum from './devFacTableSum'
import Grid from '@material-ui/core/Grid'
import useGetAllRowsById from '../../hook-querries/useGetAllRowsById'
import { useParams } from "react-router-dom"
import { useMutation } from "react-query";
import axios from 'axios'
import Button from '@material-ui/core/Button';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import MailIcon from '@material-ui/icons/Mail';
import { NavLink } from 'react-router-dom'



export default function DevFac() {
    

    const { id: documentId } = useParams()
    const patchDocument = useMutation(newDoc => axios.patch(`https://ads-compta.herokuapp.com/api/document/${documentId}`, newDoc))
    const { data, status } = useGetAllRowsById(documentId)

    const [newDoc, setNewDoc] = useState({
        "totalHT": '',
        "totalTva": '',
        "totalDiscount": '',
        "totalTTC": '',
    })
    const handleclick = () => {
        patchDocument.mutate(newDoc)
    }
   
    const mailHandleclick = () => {
      //  console.log("clic sur le mail")
    }
   

    return (
        <>
            {status === 'error' && (<p>error fetching data</p>)}
            {status === 'loading' && (<p>loading data</p>)}
            {status === 'success' && (
                <Grid item
                    container
                    xs={10} direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                    style={{ paddingTop: '40px' }}
                >
                    
                    <Grid item xs={12}>
                        <DevFacHeader />
                    </Grid>
                    <Grid item xs={12}>
                        <DevFacTable
                            data={data}
                        />
                    </Grid>
                    <Grid item xs={12} align="right">
                        <DevFacTableSum
                            data={data}
                            setNewDoc={setNewDoc}
                        />
                    </Grid>
                  
                    <Grid item align="right" style={{ marginTop: "20px" }}>
                        <Button style={{ marginRight: '10px' }} onClick={handleclick} variant="contained" color="primary">enregistrer</Button>
                        <NavLink to={`/pdfPreview/${documentId}`}>                                             
                                            
                        <Button style={{ marginRight: '10px' }} color="primary" variant="contained"><PictureAsPdfIcon /></Button>
                        </NavLink>
                        <Button style={{ marginRight: '10px' }} color="primary" variant="contained" onClick={mailHandleclick} ><MailIcon /></Button>
                    </Grid>
                </Grid>
            )}
        </>
    )
}