import { render, screen } from './test-utils/test-utils'
import App from './App';

test('homepage is ready', () => {
  render(<App />);
  const logoElement = screen.getByRole('img', {name: "logoApp"});
  expect(logoElement).toBeInTheDocument();
});

