import { ReactQueryDevtools } from 'react-query/devtools'
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'

import logo from './logoapp.png';
import {
  Switch,
  Route,
} from "react-router-dom";

import Grid from '@material-ui/core/Grid';

import Document from './pages/documents/document'
import Client from './pages/client/client'
import Site from './pages/site/site'
import Exercice from './pages/exercice/exercice'
import Parametres from './pages/parametres/parametres'
import Header from './components/Header/header'
import DevFac from './components/devFac/devFac'
import Footer from './components/footer/footer'
import ClientDetails from './pages/client/clientDetails'
import PdfPreview from './components/pdf/pdf';
import Welcome from './pages/welcome/welcome';

// Create a client
const queryClient = new QueryClient()

  function App() {
    return (
      <QueryClientProvider client={queryClient}>
      
        <div style={{ position: 'relative', minHeight: '100vh', paddingBottom: "120px" }}>
          <Switch>
          <Route exact path="/">
              <Welcome />            
              </Route>
              <>
         <Header />
          <Grid
            item
            container
            xs={12}
            direction="row"
            justify="center"
            alignItems="flex-start" >             
          

              <Route exact path="/">
              <Welcome />            
              </Route>

              <Route exact path="/documents">
              <Document />
              </Route>

              <Route exact path="/client">
                <Client />
              </Route>

              <Route exact path="/site">
                <Site />
              </Route>

              <Route path="/exercice">
                <Exercice />
              </Route>             
              <Route exact path="/document/:id">
                <DevFac />
              </Route>
              <Route path="/client/:id">
                <ClientDetails />
              </Route>
              <Route path="/parametres">
                <Parametres />
              </Route>
              <Route path="/pdfPreview/:id">
                <PdfPreview />
              </Route>
          </Grid>
          <Footer />
          </>
          </Switch>
        </div>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    );
  }

export default App;


export function Homepage() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logoApp" />
        <h1 style={{ color: "black" }}>
          ADS-Compta
        </h1>
      </header>
    </div>
  );
}



