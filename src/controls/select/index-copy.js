import React from "react";
import { useFormContext, Controller } from "react-hook-form";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { InputLabel, Select } from "@material-ui/core";


function FormSelect(props) {
  const { control } = useFormContext();
  const { name, label, options } = props;
  return (
<Controller
  as={
    <Select>
     {options.map((option, index) => (
      <MenuItem key={index} value={option}>
        {option}
      </MenuItem>
     ))}
    </Select>
    }
  name={name}
  control={control}
  defaultValue=""
  rules={{ required: true }}
  render={({
        field: { onChange, onBlur, value, name, ref },
        fieldState: { invalid, isTouched, isDirty, error },
        formState,
      }) => (
        <Select
          variant="outlined"
        />
      )}
/>


  )
}

   

export default FormSelect;

// render={({
//         field: { onChange, onBlur, value, name, ref },
//         fieldState: { invalid, isTouched, isDirty, error },
//         formState,
//       }) => (
//         <Select
//           variant="outlined"
//         />
//       )}
