import React from "react";
import { useFormContext, Controller } from "react-hook-form";
import Select from "react-select";


function FormSelect(props) {
  const { control } = useFormContext();
  const { name, label, options } = props;
  return (
    <Controller
      as={Select}
      options={options}
      name={name}
      control={control}
      render={({ field }) => <Select       
          {...field} 
         />}
    />
  )
}

export default FormSelect;
