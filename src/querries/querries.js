
//DOCUMENTS
export const fetchDocs = async () => {

    const res = await fetch('https://ads-compta.herokuapp.com/api/document',
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('jwt'),
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            },
        })
    return res.json();
};


export const fetchDocById = async (id) => {
    const res = await fetch(`https://ads-compta.herokuapp.com/api/document/${id}`,
    {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('jwt')
        },
    })
    return res.json();
};

export const deleteDoc = (id) => {
    const url = `https://ads-compta.herokuapp.com/api/document/${id}`;
    return fetch(url, {
        method: "DELETE",
        headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.getItem('jwt')
            },     
    });
};

//DOCTYPE
export const fectchDocType = async () => {
    const res = await fetch('https://ads-compta.herokuapp.com/api/docType/')
    return res.json();
}

// TVA
export const fetchTva = async () => {
    const res = await fetch('https://ads-compta.herokuapp.com/api/tva/')
    return res.json();
};

//CLIENTS

export const fetchClients = async () => {
    const res = await fetch('https://ads-compta.herokuapp.com/api/client')
    return res.json();
};

export const fetchClientById = async (id) => {
    const res = await fetch(`https://ads-compta.herokuapp.com/api/client/${id}`)
    return res.json();
};

export const postClient = (newClient) => {
    const url = "https://ads-compta.herokuapp.com/api/client";
    return fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(newClient)
    });
};

//ROWS
export const patchRow = ({ id, documentId, changes }) => {
    const url = `https://ads-compta.herokuapp.com/api/document/${documentId}/row/${id}`;
    return fetch(url, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(changes)
    });
};

export const postRow = ({ documentId, changes }) => {
    const url = `https://ads-compta.herokuapp.com/api/document/${documentId}/row`;
    return fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(changes)
    });
};

export const deleteRow = ({ id, documentId }) => {
    const url = `https://ads-compta.herokuapp.com/api/document/${documentId}/row/${id}`;
    return fetch(url, {
        method: "DELETE",
    });
};


// API PN

export const fetchApi = async (keyword) => {
    const token = 'abcdef'
    const url = `https://app-67fd1eb3-90a0-421e-a58b-b56a3ce596c1.cleverapps.io/api/parts/search?keyword=${keyword}`
    const res = await fetch(url, {
        headers: {
            'Authorization': 'Bearer ' + token,
        },
    });
    return res.json();
}


// USER
export const postUser = (newUser) => {
    const url = "http://localhost:9090/login";
    return fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(newUser)
    });
};