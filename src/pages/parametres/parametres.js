import React from 'react'
import Grid from '@material-ui/core/Grid'
import MainChanges from '../../components/parametres/mainChanges'
import NewChanges from '../../components/parametres/newChanges'

function Parametres(){
    return (
        // status === 'error' && (<p>error fetching data</p>)
        // status === 'loading' && (<p>loading data</p>)
        <Grid item
            container
            xs={10}
            direction="row"
            style={{paddingTop:'40px'}}
            > 
            <MainChanges />
            <NewChanges />
            </Grid>
       
    )
}

export default Parametres