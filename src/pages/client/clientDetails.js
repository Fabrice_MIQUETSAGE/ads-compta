import Grid from '@material-ui/core/Grid'
import { useQuery } from 'react-query'
import DocList from '../../components/devFac/docList'
import ClientDetailsHeader from '../../components/client/clientDetailsHeader'
import { useParams } from "react-router-dom"
import {fetchClientById} from '../../querries/querries'

export default function ClientDetails() {
    const { id } = useParams()
    const {data, status} = useQuery(['clientById', id], () => fetchClientById(id))

   

    return (
        <>
            {status === 'error' && (<p>error fetching data</p>)}
            {status === 'loading' && (<p>loading data</p>)}
            {status === 'success' && (
                <Grid item
                    container
                    xs={10} direction="row"
                    justify="flex-start"
                    alignItems="flex-start"
                    style={{ paddingTop: '40px' }}
                >
                    <ClientDetailsHeader
                        details={data ? data[0] : {}}
                    />

                    { data[1].length !== 0 ?
                        <DocList
                            documents={data ? data[1] : {}}
                        />
                        :
                        <h2>aucun document pour ce client</h2>
                    }
                </Grid>
            )}
        </>


    )
}