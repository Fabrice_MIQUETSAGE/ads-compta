import { render, screen } from '@testing-library/react';
import Client from './client';

test('client page is ready', () => {
  render(<Client />);
  const clientTitle = screen.getByRole("table", {name: /client$/i} )
  expect(clientTitle).toBeInTheDocument();
});
