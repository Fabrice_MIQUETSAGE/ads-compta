import React from 'react';
import ClientList from '../../components/client/clientList'
import Grid from '@material-ui/core/Grid'
import { useQuery } from 'react-query'
import { fetchClients } from '../../querries/querries'



function Client() {

    const { data, status } = useQuery('clients', fetchClients)
    

    
   if(status === 'error') return <p>error fetching data</p>
   if(status === 'loading') return <p>loading data</p>


    return (

        <Grid item
            container
            xs={10} direction="row"
            justify="flex-start"
            alignItems="flex-start"
            style={{ paddingTop: '40px' }}
        >
            <ClientList
                data={data}
            />
        </Grid>

    )
}

export default Client;