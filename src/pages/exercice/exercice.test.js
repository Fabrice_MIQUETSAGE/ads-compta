import { render, screen } from '@testing-library/react';
import Exercice from './exercice';

test('exercice page is ready', () => {
  render(<Exercice />);
  const exerciceTitle = screen.getByRole("table", {name: /exercice$/i} )
  expect(exerciceTitle).toBeInTheDocument();
});
