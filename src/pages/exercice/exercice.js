import React from 'react'
import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

//import DocList from '../../components/devFac/docList'
import ExerciceList from '../../components/exercice/exerciceList'

function Exercice() {
    // status === 'error' && (<p>error fetching data</p>)
    // status === 'loading' && (<p>loading data</p>)
    return (
        <Grid item
            container
            xs={10} direction="row"
            justify="flex-start"
            alignItems="flex-start"
            style={{ paddingTop: '40px' }}
        >
            <ExerciceList />
            <Grid item xs={12}>
                <h2 style={{ color: "#3f51b5", textTransform: "uppercase", margin: "0" }}>documents disponibles pour : ANNECY</h2>
            </Grid>
            <Grid item xs={12}>
                <h2 style={{ color: "#3f51b5", textTransform: "uppercase", margin: "0" }}>exercice comptable : EXE2021</h2>
            </Grid>
            <TableContainer>
                <Table stickyHeader aria-label="devfac table">
                    <TableHead>
                        <TableRow>
                            <TableCell>TYPE</TableCell>
                            <TableCell>NUMERO</TableCell>
                            <TableCell>SITE</TableCell>
                            <TableCell>CLIENT</TableCell>
                            <TableCell>DATE</TableCell>
                            <TableCell>STATUT</TableCell>
                            <TableCell>AUTEUR</TableCell>
                            <TableCell>TOTAL €(TTC)</TableCell>
                            <TableCell>VOIR</TableCell>
                            <TableCell>SUPPRESSION</TableCell>
                        </TableRow>
                    </TableHead>
                </Table>
            </TableContainer>
            {/* <DocList /> */}
        </Grid>

    )
}

export default Exercice