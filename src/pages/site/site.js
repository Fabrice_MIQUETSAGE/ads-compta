import React from 'react'
import SiteList from '../../components/site/siteList'
import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import DocList from '../../components/devFac/docList'

function Site() {


    // status === 'error' && (<p>error fetching data</p>)
    // status === 'loading' && (<p>loading data</p>)
    return (
        <Grid item
            container
            xs={10} direction="row"
            justify="flex-start"
            alignItems="flex-start"
            style={{ paddingTop: '40px' }}
        >

            <SiteList />
            <h2 style={{ color: "#3f51b5", textTransform: "uppercase" }}>documents disponibles pour le site d'ANNECY </h2>
            {/* <DocList /> */}
            <TableContainer>
                <Table stickyHeader aria-label="devfac table">
                    <TableHead>
                        <TableRow>
                            <TableCell>TYPE</TableCell>
                            <TableCell>NUMERO</TableCell>
                            <TableCell>SITE</TableCell>
                            <TableCell>CLIENT</TableCell>
                            <TableCell>DATE</TableCell>
                            <TableCell>STATUT</TableCell>
                            <TableCell>AUTEUR</TableCell>
                            <TableCell>TOTAL €(TTC)</TableCell>
                            <TableCell>VOIR</TableCell>
                            <TableCell>SUPPRESSION</TableCell>
                        </TableRow>
                    </TableHead>
                </Table>
            </TableContainer>
        </Grid>
    )
}

export default Site