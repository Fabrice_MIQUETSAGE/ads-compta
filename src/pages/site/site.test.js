import { render, screen } from '@testing-library/react';
import Site from './site';

test('site page is ready', () => {
  render(<Site />);
  const siteTable = screen.getByRole("table", {name: 'site liste'} )
  expect(siteTable).toBeInTheDocument();
});
