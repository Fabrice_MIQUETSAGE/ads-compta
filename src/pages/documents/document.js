import Grid from '@material-ui/core/Grid';
import DocList from '../../components/devFac/docList'
import ModalDoc from '../../components/devFac/modalDoc'
import { useQuery } from 'react-query'
import logo from '../../logoapp2.png';

//import querries
import { fetchDocs } from '../../querries/querries'


function Document() {


    const { data, status } = useQuery('docs', fetchDocs)

console.log(data)
   if(status === 'error') return <p>error fetching data</p>
   if(status === 'loading') return <p>loading data</p>

    return (
        <Grid item
            container
            xs={10} direction="row"
            justify="flex-start"
            alignItems="flex-start"
            style={{ paddingTop: '40px' }}
        >
            <Grid item container alignItems="flex-start" style={{ marginBottom: '50px', boxShadow: "0px 0px 20px 2px rgba(0,0,0,0.10)" }}>
            <Grid item xs={2} style={{paddingRight:'10px', paddingLeft:'10px', marginRight:"20px"}}>
                    <img src={logo} style={{ width: "100%", marginTop: '20px', marginBottom: '20px', boxShadow: "0px 0px 10px 2px rgba(0,0,0,0.20)" }} alt="logoApp" />
                </Grid>
                <Grid item xs={8} style={{ marginBottom: '50px' }}>
                    <h1 style={{ marginBottom: '10px' }}>Bonjour admin</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis eros id erat vulputate commodo. Ut et commodo magna, eget pellentesque ex. Quisque a nulla eu erat bibendum condimentum quis eu orci. Duis rutrum, erat vitae cursus venenatis, dui lacus maximus lectus,<br /><br /> a vestibulum orci ipsum a arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis eros id erat vulputate commodo. Ut et commodo magna, eget pellentesque ex. Quisque a nulla eu erat bibendum condimentum quis eu orci. Duis rutrum, erat vitae cursus venenatis, dui lacus maximus lectus, a vestibulum orci ipsum a arcu. </p>
                </Grid>
                
            </Grid>

            <Grid item xs={8}>
                <ModalDoc />
            </Grid>

            <Grid item container style={{ paddingTop: "40px" }}>
                <DocList
                    documents={data}
                />
            </Grid>


        </Grid>

    )
}
export default Document