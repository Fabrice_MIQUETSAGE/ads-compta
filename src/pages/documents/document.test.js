import {render, screen} from '../../test-utils/test-utils'
import Document from './document'

test('document page is ready', ()=> {
    render(<Document/>)
        
    //doit avoir un bouton nouveau devis
    const documentButtonDevis = screen.getByRole("link", {name: /nouveau devis/i})
    expect(documentButtonDevis).toBeInTheDocument();

    //doit avoir un bouton nouvelle facture
    const documentButtonFacture = screen.getByRole("link", {name: /nouvelle facture/i})
    expect(documentButtonFacture).toBeInTheDocument();

    //doit avoir une table
    const documentTable = screen.getByRole("table", {name: /sticky table/i})
    expect(documentTable).toBeInTheDocument();
})