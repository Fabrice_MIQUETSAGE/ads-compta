import Grid from '@material-ui/core/Grid'
import logo from '../../logoapp2.png';
import LogForm from '../../components/auth/logForm'

export default function Welcome() {


    return (

        <Grid item container  direction="row" xs={12} justify="center" align-items="center">
            <Grid item xs={8} align="center">
                <img src={logo} style={{ width: "30%", marginTop:'100px', marginBottom:'50px', boxShadow:"0px 0px 19px 5px rgba(0,0,0,0.10)" }} alt="logoApp" />
        </Grid>
            <Grid item xs={8} align="center">
            <h2>Bienvenue !</h2>
            <h3>veuillez vous identifier : </h3>
               <LogForm />
            </Grid>
            
        </Grid>



    )
}